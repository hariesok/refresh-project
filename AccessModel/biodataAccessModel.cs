﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using DataModel;

namespace AccessModel
{
    public class biodataAccessModel
    {
        public static string Message = string.Empty;

        public static bool Insert(biodataViewModel data) {
            Message = string.Empty;
            bool result = false;
            try
            {
                using (var db = new refresh_dbEntities()) {
                    biodata add = new biodata();
                    add.Email = data.Email;
                    add.Nama = data.Nama;
                    add.NPM = data.NPM;
                    add.Umur = data.Umur;
                    add.Jenis_Kelamin = data.Jenis_Kelamin;
                    add.Telp = data.Telp;
                    add.Alamat = data.Alamat;
                    add.Asal = data.Asal;
                    add.Domisili = data.Domisili;
                    add.created_by = data.created_by;
                    add.created_on = DateTime.Now;
                    add.deleted = false;
                    db.biodatas.Add(add);
                    db.SaveChanges();
                }
                result = true;
                Message = "Data berhasil disimpan.";
            }
            catch (Exception hasError)
            {
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else {
                    Message = "Maaf terjadi kesalahan. Silahkan coba beberapa saat lagi.";
                }
            }
            return result;
        }

        public static List<biodataViewModel> GetListAll() {
            List<biodataViewModel> result = new List<biodataViewModel>();
            using (var db = new refresh_dbEntities()) {
                result = (from d in db.biodatas
                          where d.deleted == false
                          select new biodataViewModel
                          {
                              id = d.id,
                              Nama = d.Nama,
                              NPM = d.NPM,
                              Alamat = d.Alamat,
                              Email = d.Email
                          }
                    ).ToList();
            } return result;
        }

        public static biodataViewModel GetDetails(long Id) {
            Message = string.Empty;
            biodataViewModel result = new biodataViewModel();

            using (var db = new refresh_dbEntities()) {
                result = (
                    from a in db.biodatas
                    where a.id == Id && a.deleted == false
                    select new biodataViewModel
                    {
                        id = a.id,
                        Email = a.Email,
                        Nama = a.Nama,
                        NPM = a.NPM,
                        Umur = a.Umur,
                        Jenis_Kelamin = a.Jenis_Kelamin,
                        genderName = a.Jenis_Kelamin==true? "Laki-laki":"Perempuan",
                        Telp = a.Telp,
                        Alamat = a.Alamat,
                        Asal = a.Asal,
                        Domisili = a.Domisili,
                        created_on = a.created_on
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Update(biodataViewModel data) {
            Message = string.Empty;
            bool result = false;
            try
            {
                using (var db = new refresh_dbEntities()) {
                    biodata u = db.biodatas.Where(o => o.id == data.id).FirstOrDefault();
                    if (u != null)
                    {
                        u.Email = data.Email;
                        u.Nama = data.Nama;
                        u.NPM = data.NPM;
                        u.Umur = data.Umur;
                        u.Jenis_Kelamin = data.Jenis_Kelamin;
                        u.Telp = data.Telp;
                        u.Alamat = data.Alamat;
                        u.Asal = data.Asal;
                        u.Domisili = data.Domisili;
                        db.SaveChanges();
                        result = true;
                    }
                    else {
                        Message = "data tidak ditemukan.";
                    }
                }
                Message = "Data berhasil diupdate.";
            }
            catch (Exception hasError)
            {
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Delete(long Id) {
            Message = string.Empty;
            bool result = false;
            try
            {
                using (var db = new refresh_dbEntities()) {
                    biodata del = db.biodatas.Where(o => o.id == Id).FirstOrDefault();
                    if (del != null)
                    {
                        del.deleted = true;
                        del.deleted_by = 1;
                        del.deleted_on = DateTime.Now;

                        db.SaveChanges();
                        Message = "Data berhasil dinonaktifkan.";
                        result = true;
                    }
                    else {
                        Message = "Data tidak ditemukan.";
                    }
                }
            }
            catch (Exception hasError)
            {
                if (hasError != null)
                {
                    if (hasError.Message.ToLower().Contains("inner exception"))
                    {
                        Message = hasError.InnerException.InnerException.Message;
                    }
                    else
                    {
                        Message = "Maaf terjadi kesalahan.";
                    }
                }
            }
            return result;
        }
    }
}
