﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ViewModel
{
    public class biodataViewModel
    {
        public long id { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan alamat email anda.")]
        [EmailAddress(ErrorMessage = "Silahkan masukkan email sesuai format.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan nama anda.")]
        public string Nama { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan NPM anda.")]
        public string NPM { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan usia anda.")]
        public int Umur { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan data jenis kelamin anda.")]
        public bool Jenis_Kelamin { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan nomor telepon anda.")]
        [Phone(ErrorMessage = "Mohon masukkan angka untuk nomor hape.")]
        public string Telp { get; set; }
        [Required(ErrorMessage = "Anda belum memasukkan alamat.")]
        public string Alamat { get; set; }
        [Required(ErrorMessage = "Silahkan masukkan alamat asal anda.")]
        public string Asal { get; set; }
        [Required(ErrorMessage = "Silahkan pilih domisili anda.")]
        public string Domisili { get; set; }
        public System.DateTime created_on { get; set; }
        public long created_by { get; set; }
        public bool deleted { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public Nullable<long> deleted_by { get; set; }


        public string genderName {get; set;}
    }
}
