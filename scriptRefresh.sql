USE [master]
GO
/****** Object:  Database [refresh_db]    Script Date: 7/15/2020 3:20:34 PM ******/
CREATE DATABASE [refresh_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'refresh_db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\refresh_db.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'refresh_db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER2012\MSSQL\DATA\refresh_db_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [refresh_db] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [refresh_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [refresh_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [refresh_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [refresh_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [refresh_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [refresh_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [refresh_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [refresh_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [refresh_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [refresh_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [refresh_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [refresh_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [refresh_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [refresh_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [refresh_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [refresh_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [refresh_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [refresh_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [refresh_db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [refresh_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [refresh_db] SET RECOVERY FULL 
GO
ALTER DATABASE [refresh_db] SET  MULTI_USER 
GO
ALTER DATABASE [refresh_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [refresh_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [refresh_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [refresh_db] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'refresh_db', N'ON'
GO
USE [refresh_db]
GO
/****** Object:  Table [dbo].[biodata]    Script Date: 7/15/2020 3:20:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[biodata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](30) NOT NULL,
	[Nama] [varchar](50) NOT NULL,
	[NPM] [varchar](10) NOT NULL,
	[Umur] [int] NOT NULL,
	[Jenis_Kelamin] [bit] NOT NULL,
	[Telp] [varchar](15) NOT NULL,
	[Alamat] [varchar](100) NOT NULL,
	[Asal] [varchar](100) NOT NULL,
	[Domisili] [varchar](20) NOT NULL,
	[created_on] [datetime] NOT NULL,
	[created_by] [bigint] NOT NULL,
	[deleted] [bit] NOT NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[biodata] ON 

INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (1, N'me@hariesok.co.uk', N'Haries Chriestian', N'13115056', 23, 1, N'0123456789', N'Tangsel', N'Tangsel', N'Tangsel', CAST(0x0000ABF900000000 AS DateTime), 1, 0, NULL, NULL)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (2, N'haries@hariesok.co.uk', N'Hari Esok', N'0123456', 23, 1, N'34567', N'Jaksel', N'Tangsel', N'Karawang', CAST(0x0000ABF900000000 AS DateTime), 1, 1, CAST(0x0000ABF900F72E24 AS DateTime), 1)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (3, N'Johnny@gmail.com', N'Si Joni', N'09876543', 24, 1, N'0123456789', N'Jaksel', N'Indonesia', N'Karawang', CAST(0x0000ABF900B5D15B AS DateTime), 1, 0, NULL, NULL)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (4, N'Johnny@gmail.com', N'Johnny Virtue', N'0345678', 26, 1, N'098765432', N'NYE', N'NYE', N'NYE', CAST(0x0000ABF900BD69BC AS DateTime), 1, 0, NULL, NULL)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (5, N'me@hariesok.co.uk', N'Johnny Sasaki', N'0987654', 29, 1, N'0987654', N'Tokyo', N'Japan', N'Karawang', CAST(0x0000ABF900CDBE21 AS DateTime), 1, 0, NULL, NULL)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (6, N'me@hariesok.co.uk', N'Solid Snake', N'0987655', 35, 1, N'0987654', N'NYE (New York Eity)', N'Washington D.C.', N'Karawang', CAST(0x0000ABF900CE1DF7 AS DateTime), 1, 0, NULL, NULL)
INSERT [dbo].[biodata] ([id], [Email], [Nama], [NPM], [Umur], [Jenis_Kelamin], [Telp], [Alamat], [Asal], [Domisili], [created_on], [created_by], [deleted], [deleted_on], [deleted_by]) VALUES (19, N'me@hariesok.co.uk', N'Meryl Silverburgh', N'098765432', 21, 0, N'09876543', N'NYE', N'Karawaci', N'Karawang', CAST(0x0000ABF900E106FB AS DateTime), 1, 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[biodata] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__biodata__C7DE9A2EFC3A72B6]    Script Date: 7/15/2020 3:20:34 PM ******/
ALTER TABLE [dbo].[biodata] ADD UNIQUE NONCLUSTERED 
(
	[NPM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[biodata] ADD  DEFAULT ('Karawang') FOR [Domisili]
GO
ALTER TABLE [dbo].[biodata] ADD  DEFAULT ('false') FOR [deleted]
GO
USE [master]
GO
ALTER DATABASE [refresh_db] SET  READ_WRITE 
GO
