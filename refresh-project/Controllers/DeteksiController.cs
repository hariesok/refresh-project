﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccessModel;
using ViewModel;

namespace refresh_project.Controllers
{
    public class DeteksiController : Controller
    {
        public static string messageTemp { get; set; }
        public ActionResult IndexRedirector(string message) {
            messageTemp = message;
            return RedirectToAction("Index");
        }
        // GET: Deteksi
        public ActionResult Index()
        {
            ViewBag.alertMessage = messageTemp;
            messageTemp = string.Empty;
            return View("Index");
        }

        public ActionResult Add() {
            return View();
        }

        [HttpPost]
        public ActionResult Add(biodataViewModel data) {
            data.created_by = 1;
            bool AddResult = biodataAccessModel.Insert(data);
            if (AddResult == true)
            {
                return RedirectToAction("IndexRedirector", new { message = "Data berhasil disimpan." });
            }
            else {
                ViewBag.Message = biodataAccessModel.Message;
                return View();
            }
        }

        public ActionResult List() {
            var list = biodataAccessModel.GetListAll();
            return PartialView(list);
        }

        public ActionResult Edit(long Id) {
            return View(biodataAccessModel.GetDetails(Id));
        }
        [HttpPost]
        public ActionResult Edit(biodataViewModel data) {
            bool editResult = biodataAccessModel.Update(data);
            if (editResult == true)
            {
                return RedirectToAction("IndexRedirector", new { message = "Data berhasil diubah." });
            }
            else {
                ViewBag.Message = biodataAccessModel.Message;
                return View();
            }
        }

        public ActionResult Details(long Id) {
            biodataViewModel result = new biodataViewModel();
            result = biodataAccessModel.GetDetails(Id);
            return PartialView(result);
        }
        public ActionResult Delete(long Id) {
            bool delResult = biodataAccessModel.Delete(Id);
            if (delResult == true)
            {
                return RedirectToAction("IndexRedirector", new { message = "Data berhasil dinonaktifkan." });
            }
            else { 
                return RedirectToAction("IndexRedirector", new { message = "Mohon maaf terjadi kesalahan, silakan coba lagi." });
            }
        }
    }
}